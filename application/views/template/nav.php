<div class="wrapper">
    <nav id="sidebar" class="sidebar fixed-top">
        <div class="sidebar-header">
            <a href="<?= base_url('home') ?>" class="side-title">
                <h4 class="text-white">TUKUBUKU</h4>
            </a>
        </div>
        <ul class="list-unstyled component">
            <li class="active"><a href="<?= base_url('home/index') ?>" class="btn-block"><span><i
                            class="fas fa-tachometer-alt"></i></span>Dashboard</a></li>
            <li class="active"><a href="<?= base_url('produk/index') ?>" class="btn-block"><span><i
                            class="fas fa-box"></i></span>Product</a></li>
        </ul>
    </nav>