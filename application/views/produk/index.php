<div id="content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="content-title mt-2">Produk</h3>
            </div>
            <div class="token" style="display: none;"></div>
            <div class="col-lg-12">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#form">
                    Add Product
                </button>

                <!-- Modal -->
                <div class="modal fade" id="form" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                        <div class="modal-content">
                            <div class="modal-header biru">
                                <h5 class="modal-title">Add New Product</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="nama">Name</label>
                                            <input type="text" class="form-control" autofocus id="nama">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="desk">Description</label>
                                            <input type="text" class="form-control" id="desk"
                                                placeholder="simple description">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="harga">Price</label>
                                            <input type="text" class="form-control" autocomplete="off" id="harga"
                                                placeholder="200000">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <img src="<?= base_url('assets/img') ?>/buy.png" class="img-fluid mt-2 gambar"
                                            alt="" data-base="">
                                        <button class="btn btn-block biru gambarbtn mt-2"><i
                                                class="fas fa-image mr-2"></i> Choose Image</button>
                                        <input type="file" style="display:none" id="gambar" accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn biru add" data-id="">Add</button>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="main-content mt-4">
                    <div class="container">
                        <div class="row isi">

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>