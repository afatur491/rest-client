<?php
class ProdukModel extends CI_Model
{
    public $url;
    public function __construct()
    {
        parent::__construct();
        $this->url = "http://localhost/mjp-web-service-ci/product";
    }
    public function getproduk($key)
    {
        $token = array();
        $token[] = "X-Auth: $key";
        curl_setopt(curl_init(), CURLOPT_URL, $this->url);
        curl_setopt(curl_init(), CURLOPT_RETURNTRANSFER, 1);
        curl_setopt(curl_init(), CURLOPT_HTTPHEADER, $token);
        $result = curl_exec(curl_init());
        curl_close(curl_init());
        return json_decode($result, true);
    }
}