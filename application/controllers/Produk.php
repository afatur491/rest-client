<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Produk extends CI_Controller
{
    public $token;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }
    public function index()
    {
        $data['title'] = 'Produk';
        $this->load->view('template/top', $data);
        $this->load->view('template/nav');
        $this->load->view('produk/index', $data);
        $this->load->view('template/bottom');
    }
}