<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Home extends CI_Controller
{
	public $token;
	public $client;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxNTk1NTYwNTYxLCJuYmYiOjE1OTU1NjA1NzEsImRhdGEiOnsiaWQiOjMsIm5hbWEiOiJld2FlIiwiZW1haWwiOiJhQGEuY29tIn19.lRIq9fwkusUWIi8swwDZUjBCT57F-dM6znJgo9F9ZGw";
	}
	public function index()
	{
		$data['title'] = 'Home';
		$data['token'] = $this->token;
		$this->load->view('template/top', $data);
		$this->load->view('template/nav');
		$this->load->view('home/index', $data);
		$this->load->view('template/bottom');
	}
}