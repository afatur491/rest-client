$(document).ready(function () {
	var url = "http://localhost/mjp-web-service-ci/";
	var token =
		"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxNTk1NTU1MTI1LCJuYmYiOjE1OTU1NTUxMzUsImRhdGEiOnsiaWQiOjEsIm5hbWEiOiJBY2htYWQgRmF0dXIiLCJlbWFpbCI6ImFmYXR1cjQ5MUBnbWFpbC5jb20ifX0.-pofVyzazFrVbCzDFHcDvZSJSfBsCJdcq129OtF7l54";
	getpro();
	var img = url + "assets/img/buy.png";
	//triger to input:file
	$(".gambarbtn").click(function () {
		$("#gambar").click();
	});
	// clear textbox
	$("#form").on("hidden.bs.modal", function () {
		$("#nama").val("");
		$("#desk").val("");
		$("#harga").val("");
		$(".gambar").attr("src", "http://localhost/rest-client/assets/img/buy.png");
		$(".add").html("Add");
	});

	// change gambar
	$("#gambar").change(function () {
		if (this.files && this.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$(".gambar").attr("src", e.target.result);
				$(".token").text(e.target.result);
			};

			reader.readAsDataURL(this.files[0]); // convert to base64 string
		}
	});
	// add and edit produk
	$(".add").click(function () {
		var gambar = $(".token").text();
		var nama = $("#nama").val();
		var desk = $("#desk").val();
		var idp = $(this).data("id");
		var harga = $("#harga")
			.val()
			.substring(4, $("#harga").val().length - 3)
			.replace(/,/g, "");
		if ($(this).html() == "Add") {
			var data = {
				nama: nama,
				deskripsi: desk,
				harga: harga,
				gambar: gambar,
			};
			// console.log(data);
			tambah(data);
			$("#gambar").val("");
		} else if ($(this).html() == "Edit") {
			var data = {
				nama: nama,
				deskripsi: desk,
				harga: harga,
				gambar: gambar,
				id: idp,
			};
			ubah(data);
			$("#gambar").val("");
		}
	});
	//change format number
	$("#harga").blur(function () {
		let harga = new Intl.NumberFormat("en-US", {
			style: "currency",
			currency: "IDR",
		});
		if ($(this).val() != "") {
			$(this).val(harga.format($(this).val()));
		}
	});
	// reverse change format number
	$("#harga").focus(function () {
		let isi = $(this)
			.val()
			.substring(4, $(this).val().length - 3)
			.replace(/,/g, "");
		$(this).val(isi);
	});
	//price textboxonly number
	$("#harga").keydown(function (e) {
		var key = e.charCode || e.keyCode || 0;
		return (
			key == 8 ||
			key == 9 ||
			key == 13 ||
			key == 46 ||
			key == 110 ||
			key == 190 ||
			(key >= 35 && key <= 40) ||
			(key >= 48 && key <= 57) ||
			(key >= 96 && key <= 105)
		);
	});
	//function for get the product
	function getpro() {
		$.ajax({
			url: url + "product",
			method: "get",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			success: function (data) {
				var produk = data.data;
				let isi = "";
				// console.log(produk[1].nama);
				$.each(produk.reverse(), function (i, pro) {
					isi = dalam(pro);
					$(".isi").append(isi);
				});
				$(".edit").click(function () {
					var id = $(this).data("id");
					getprobyid(id);
				});
				$(".hapus").click(function () {
					var id = {
						id: $(this).data("id"),
					};
					Swal.fire({
						title: "Are you sure?",
						text: "You won't be able to revert this!",
						icon: "warning",
						showCancelButton: true,
						confirmButtonColor: "#3085d6",
						cancelButtonColor: "#d33",
						confirmButtonText: "Yes, delete it!",
					}).then((result) => {
						if (result.value) {
							hapus(id);
						}
					});
				});
			},
		});
	}
	// fuction for add product
	function tambah(data) {
		$.ajax({
			url: url + "product/add",
			method: "post",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			data: data,
			success: function (data) {
				if (data.status === 1) {
					Swal.fire({
						icon: "success",
						title: "Success",
						text: data.pesan,
					});
					$(".isi").html("");
					getpro();
					$(".token").text("");
					$("#form").modal("toggle");
				} else if (data.status == 0) {
					Swal.fire({
						icon: "error",
						title: "Failed",
						text: data.data,
					});
					$(".token").text("");
					// console.log("gagal");
				}
			},
		});
	}
	function ubah(data) {
		$.ajax({
			url: url + "product/edit",
			method: "put",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			data: data,
			success: function (data) {
				if (data.status === 1) {
					Swal.fire({
						icon: "success",
						title: "Success",
						text: data.pesan,
					});
					$(".isi").html("");
					getpro();
					$(".token").text("");
					$("#form").modal("toggle");
				} else if (data.status == 0) {
					Swal.fire({
						icon: "error",
						title: "Failed",
						text: data.data,
					});
					$(".token").text("");
					// console.log("gagal");
				}
			},
		});
	}
	// get data product by id
	function getprobyid(id) {
		$.ajax({
			url: url + "product/" + id,
			method: "get",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			success: function (data) {
				var produk = data.data;
				$("#nama").val(produk.nama);
				$("#desk").val(produk.deskripsi);
				$(".add").data("id", produk.id);
				$("#harga").val(
					new Intl.NumberFormat("en-US", {
						style: "currency",
						currency: "IDR",
					}).format(produk.harga)
				);
				$(".gambar").attr("src", url + produk.gambar);
				$(".add").html("Edit");
				$("#form").modal("toggle");
			},
		});
	}
	// function for delete product
	function hapus(data) {
		$.ajax({
			url: url + "product/delete",
			method: "delete",
			dataType: "json",
			headers: {
				"X-Auth": token,
			},
			data: data,
			success: function (data) {
				if (data.status == 1) {
					Swal.fire({
						icon: "success",
						title: "Success",
						text: data.pesan,
					});
					$(".isi").html("");
					getpro();
				} else if (data.status == 0) {
					Swal.fire({
						icon: "error",
						title: "Failed",
						text: data.pesan,
					});
				}
			},
		});
	}
	// function for loop
	function dalam(pro) {
		return `<div class="col-lg-6 col-md-6 col-sm-12">
		<div class="card mb-3" style="max-width: 480px;">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="${url + pro.gambar}" class="card-img" alt="...">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<h5 class="card-title">${pro.nama}</h5>
						<small class="text-muted">Rp ${parseFloat(pro.harga, 10)
							.toFixed(2)
							.replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
							.toString()}</small>
						<p class="card-text">${pro.deskripsi}</p>
						<a href="#" class="badge biru edit" data-id="${
							pro.id
						}"><i class="fas fa-pen"></i></a>
						<a href="#" data-id="${
							pro.id
						}" class="badge merah hapus"><i class="fas fa-trash"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>`;
	}
});
